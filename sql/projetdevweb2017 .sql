-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 11, 2017 at 03:35 PM
-- Server version: 5.7.18-0ubuntu0.16.04.1
-- PHP Version: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projetdevweb2017`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `titre` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `contenu` text COLLATE utf8_bin,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `titre`, `contenu`, `date`) VALUES
(3, 'Google not, learn not: why searching can sometimes be better than knowing', 'A few months ago, I was reading through some of the posts in my home town’s freeCodeCamp study group. I came across an intriguing post.\r\n\r\nOne of the campers expressed her frustration with not being able to immediately come up with solutions, and questioned whether this will ever gets any easier. At the end of her post, she lamented needing to google for answers — as though this were something to be ashamed of.\r\n\r\nI remember exactly what it felt like to be in that situation. I was angry with myself for not being able to rattle off code on demand. I felt like using Google to search for answers was a sign of defeat. Something that, in my mind, signified I was incapable of thinking like a programmer.\r\n\r\nFast-forward 18 months, and I’m currently employed as a full-stack Software Engineer. I spend a considerable amount of time asking Google for help with my day-to-day work. I no longer see it as a weakness when I search StackOverflow for an explanation. It’s actually one of my first steps towards a solution.', '2017-06-03 18:44:34'),
(4, 'Why do so few people major in computer scienc', 'Hong Kong-based technology writer Dan Wang wrote a blog post exploring why so few people get degrees in computer science. And yes — it’s far fewer people than those who study other science and engineering fields.\r\n\r\nHere are the most common explanations for this phenomenon, and why he finds them to be insufficient:\r\n\r\n    Computer science is hard. But it isn’t necessarily harder than other science and engineering fields, many of which are surging in popularity.\r\n    You don’t need a CS degree to be a developer. But it certainly helps you get your foot in the door at big tech companies — more so than other majors.\r\n    People aren’t so market-driven when they’re considering majors. But there’s evidence that people increasingly go into fields that are growing, like healthcare. So this also doesn’t fully explain why more people aren’t majoring in CS.\r\n\r\nHe goes on to explore a total of 11 possible contributing factors. But no single one of these factors fully explains why so few people study computer science, even when there are so many high-paying, high-prestige developer jobs out there.\r\n\r\nAnyway, if you’re currently in university, or planning to start soon, I strongly recommend majoring in computer science.\r\n\r\nProgramming really is where most of the new jobs are, and this trend will accelerate as more and more work is done by machines.', '2017-06-03 18:44:34'),
(8, 'Buttercup – Un gestionnaire de mot de passe libre qui vaut le coup d’oeil', 'Si vous cherchez un bon petit gestionnaire de mots de passe, qui ne soit pas 1password, Keepass, Dashlane ou Lastpass, je vous invite à jeter un oeil à ButterCup.\r\n\r\nEncore en beta, ce gestionnaire de mot de passe a la particularité d\'être sous licence libre et d\'être dispo pour Windows, Mac et Linux. Basé sur NodeJS, Buttercup chiffre son contenu en AES-256 grâce au mot de passe maitre que vous lui donnez, une fois seulement après que ce mot de passe ait été salé et hashé avec la fonction de dérivation de clé PBKDF2.\r\n\r\nVos mots de passe sont ainsi conservés dans des archives chiffrées localement, mais aussi à distance lorsqu\'elles sont sauvegardées sur Dropbox, ownCloud ou Nextcloud (ou autres fournisseurs de stockage supportant WebDAV).\r\n\r\nDe plus, si vous êtes plusieurs à utiliser la même base de mots de passe via Buttercup, celui-ci intègre un outil de résolution de conflits (merge) ainsi qu\'une fonctionnalité d\'importation d\'une base de mot de passe existante sous 1Password, Lastpass ou Keepass.\r\n\r\nEnfin, et je terminerai là dessus, Buttercup propose un module pour Firefox qui permet de remplir automatiquement les champs sur les formulaires de login de vos sites web préférés. Top !!\r\n\r\nIl ne lui manque qu\'une version Android / iOS pour être complet.', '2017-06-11 12:31:12'),
(9, 'Les attaques DDoS ', 'Avant de rentrer dans le vif du sujet, je vous propose de faire d\'abord une petite plongée historique non douloureuse.\r\nDans la petite ville de Champaign et Urbana dans l\'Illinois (aux États-Unis) se trouve le CERL, un laboratoire de recherche informatique. Et en face du CERL, il y a un lycée.\r\n\r\nEt si vous vous étiez rendu dans ce lycée en 1974, vous auriez pu y rencontrer David Dennis, un garçon de 13 ans passionné d\'informatique. David très curieux de nature avait entendu parler d\'une commande pouvant s\'exécuter sur les systèmes PLATO qui justement se trouvaient au CERL. PLATO était l\'un des premiers systèmes multi-utilisateurs grand public destinés à l\'éducation et la recherche.\r\n\r\nCe système a été à l\'origine de nombreux concepts modernes liés à l\'utilisation multi user comme l\'email, les chats rooms, la messagerie instantanée, les jeux multijoueurs ou le partage d\'écran.\r\n\r\nLa commande apprise par David était &quot;external&quot; ou &quot;ext&quot; qui permettait d\'interagir directement avec un périphérique externe connecté au terminal. Le truc rigolo, c\'est que si vous lanciez cette commande sur un terminal où rien n\'était branché, cela faisait crasher totalement la machine. Un reboot complet était alors nécessaire pour faire refonctionner le terminal. C\'était un peu la blague du moment et David était curieux de voir ce que ça pourrait donner si cette commande était lancée simultanément sur tous les terminaux d\'une salle pleine d\'utilisateurs.\r\n\r\nLa bonne blague quoi !\r\n\r\nIl a donc mis au point un petit programme et s\'est rendu au CERL pour le tester. Résultat, 31 utilisateurs éjectés de leur session et obligés de rebooter. C\'était la première attaque DoS (Déni de Service) de l\'histoire. Suite à ça, PLATO a été patché pour ne plus accepter &quot;ext&quot; comme une commande pouvant être exécuté à distance.\r\n\r\nA la fin des années 90, le DoS était aussi régulièrement pratiqué sur l\'IRC pour déconnecter tous les gens d\'une room afin que l\'attaquant puisse reprendre la main sur l\'administration de celle-ci en étant le premier à s\'y reconnecter.\r\n\r\nDes années plus tard, en août 1999, un hacker a mis au point un outil baptisé &quot;Trinoo&quot; afin de paralyser les ordinateurs du réseau de l\'Université du Minnesota. Trinoo était en réalité un réseau composé de quelques machines &quot;maitres&quot; auxquelles le hacker pouvait donner des instructions de DoS. Ces machines maitres faisaient ensuite suivre ces instructions à des centaines de machines &quot;esclaves&quot;, floodant massivement l\'adresse IP de la cible. Les propriétaires des machines esclaves n\'avaient aucune idée que leur machine était compromise. Ce fut l\'une des premières attaques DDoS à grande échelle.\r\n\r\nLe DDoS (Déni de Service Distribé) est devenu au fil des années la technique la plus utilisée pour paralyser des machines et le grand public a commencé à entendre ce terme en 2000 lorsque le site du FBI, eBay, Yahoo, Amazon ou encore le site de CNN en ont fait les frais lors d\'une attaque d\'une ampleur encore jamais observée à l\'époque.\r\n\r\nMaintenant la technique du DDoS est utilisée aussi bien par des cybercriminels réclamant des rançons que par des hacktivistes désireux de faire entre leur point de vue.\r\n\r\nTypes d\'attaques DDoS\r\n\r\nIl existe 3 catégories d\'attaques DDoS :\r\n\r\nLes attaques DDoS basées sur le volume\r\nLes attaques DDoS ciblant les applications\r\nLes attaques DDoS à bas volume (LDoS pour Low rate)\r\nAttaques DDoS basées sur le volume\r\n\r\nLe concept est assez classique. Il s\'agit de flooder la cible avec une grande quantité des paquets ou de connexions pour saturer tous les équipements nécessaires ou pour mobiliser toute la bande passante. Elles nécessitent des botnets, c\'est à dire des réseaux de machines zombies capables de flooder des machines de manière synchronisée.\r\n\r\nCes réseaux botnet sont contrôlés par des cybercriminels qui l\'utilisent pour leur profit ou qui le louent à l\'heure à leurs clients pour une somme abordable. Ainsi, sans compétences techniques particulières, des mafias, des employés mécontents ou des concurrents peuvent franchir le pas et paralyser un serveur.\r\n\r\nAttaques DDoS ciblant les applications\r\n\r\nUne attaque DDoS peut cibler différentes applications mais la plus commune consiste à flooder à l\'aide de requêtes GET et POST, le serveur web de la cible. Surchargé par ce flood HTTP, le serveur web ne parvient plus à répondre et le site ou le service devient inaccessible. D\'autres applications peuvent être ciblées comme les DNS.\r\n\r\nAttaques à bas volume\r\n\r\nCes attaques tirent le plus souvent parti de défaut de conception dans les applications. Avec une très petite quantité de données et de bande passante, un attaquant peut paralyser un serveur. L\'un des exemples les plus connus est celui de Slowloris, un outil développé par RSnake (Robert Hansen) qui maintient ouvertes des connexions sur le serveur cible en envoyant une requête partielle. Ces connexions non refermées qui s\'accumulent saturent alors totalement le serveur.\r\n\r\nJe vais maintenant vous détailler des méthodologies de DDoS très connues pour que vous ayez une idée du fonctionnement de celles-ci.\r\n\r\nLe Flood ICMP fait partie des attaques les plus anciennes. L\'attaquant sature la cible à l\'aide de requêtes ICMP echo (un genre de ping) ou d\'autres types, ce qui permet de mettre par terre l\'infrastructure réseau de la victime.\r\n\r\nLe Flood SYN est un autre grand classique. Il consiste à initier des connexions TCP vers le serveur à l\'aide d\'un message SYN. Le serveur prend en compte ce message et répond au client à l\'aide du message SYN-ACK. Le client doit alors répondre par un nouveau message de type ACK pour signifier au serveur que la connexion est correctement établie. Seulement voilà, dans le cadre d\'une attaque de type SYN, le client ne répond pas et le serveur garde alors la connexion ouverte. En multipliant ce genre de messages SYN il est possible de paralyser le serveur.\r\n\r\nLe Flood UDP consiste à envoyer un grand nombre de paquets UDP vers des ports aléatoire de la machine cible. Cette dernière indique alors avec un message ICMP qu\'aucune application ne répond sur ce port. Si la quantité de paquets UDP envoyés par l\'attaquant est conséquente, le nombre de messages ICMP renvoyé par la victime le sera aussi, saturant les ressources de la machine.\r\n\r\nLe Flood SIP INVITE touche les systèmes VoIP. Les messages de type SIP INVITE utilisé pour établir une session entre un appelant et un appelé sont envoyés massivement à la victime, provocant le déni de service.\r\n\r\nLes attaques Smurf sont un autre type d\'attaques basées sur ICMP qui consiste à spoofer l\'adresse IP de la victime pour émettre un grand nombre de paquets ICMP. Les machines présentes sur le réseau répondent alors à ces requêtes ICMP, ce qui sature la machine cible.\r\n\r\nLes attaques par amplification DNS consistent à envoyer, en spoofant l\'adresse IP de la victime, de petites requêtes vers un serveur DNS et d\'exiger de lui une réponse d\'une taille beaucoup plus importante. Un botnet peut alors largement amplifier la taille d\'une attaque en s\'appuyant sur des infrastructures existantes légitimes comme les serveurs DNS.\r\n\r\nPour mieux vous rendre compte de la fréquence de ces attaques, le site digitalattackmap.com propose une carte animée. Vous pourrez voir la nature, l\'origine et la destination de ces attaques DDoS qui se déroulent au moment où vous lisez ces lignes.\r\n\r\nJ\'espère que cet article vous aura plu. La semaine prochaine, je vous parlerai des différents moyens que vous avez à votre disposition pour optimiser le chargement de vos pages web.', '2017-06-11 12:33:46');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `chat_messages`
--

CREATE TABLE `chat_messages` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `message` varchar(255) COLLATE utf8_bin NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `chat_messages`
--

INSERT INTO `chat_messages` (`id`, `uid`, `message`, `date`) VALUES
(1, 1, 'Test chat', '2017-05-24 14:45:13'),
(2, 2, 'test chat 2', '2017-05-24 14:45:13'),
(3, 1, 'hello', '2017-06-02 11:37:08'),
(4, 1, 'hello', '2017-06-02 11:45:49'),
(5, 1, 'event', '2017-06-02 11:46:43'),
(6, 1, 'event', '2017-06-02 11:48:30'),
(7, 1, 'event2', '2017-06-02 11:48:43'),
(8, 1, 'event2', '2017-06-02 11:49:11'),
(9, 1, 'ev3', '2017-06-02 11:49:15'),
(10, 1, 'ev3', '2017-06-02 11:50:30'),
(11, 1, 'prevent', '2017-06-02 11:50:36'),
(12, 1, 'prevent click', '2017-06-02 11:50:49'),
(13, 1, 'slt', '2017-06-02 13:25:29'),
(14, 1, 'ca va?\n', '2017-06-02 13:25:58'),
(15, 1, 's\n', '2017-06-02 13:30:44'),
(16, 1, 'slt !\n', '2017-06-02 13:35:39'),
(17, 1, 're', '2017-06-02 13:35:58'),
(18, 1, 'hello', '2017-06-02 13:36:41'),
(19, 1, 'slt', '2017-06-02 13:45:28'),
(20, 1, 'slt', '2017-06-02 13:48:03'),
(21, 1, 'hi !\n', '2017-06-02 13:51:09'),
(22, 1, 'test\n', '2017-06-02 13:53:03'),
(23, 1, '10eme message\n', '2017-06-02 14:03:04'),
(24, 3, 'slt !', '2017-06-03 11:12:31'),
(25, 4, 'slt !', '2017-06-03 11:17:41');

-- --------------------------------------------------------

--
-- Table structure for table `commentaires`
--

CREATE TABLE `commentaires` (
  `id` int(11) NOT NULL,
  `articleid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `content` text COLLATE utf8_bin NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `paniers`
--

CREATE TABLE `paniers` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `uid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `paniers`
--

INSERT INTO `paniers` (`id`, `date`, `uid`) VALUES
(1, '2017-05-26 19:39:34', 1),
(2, '2017-06-03 11:12:19', 3),
(3, '2017-06-03 11:15:58', 4),
(4, '2017-06-11 13:21:38', 5);

-- --------------------------------------------------------

--
-- Table structure for table `panier_products`
--

CREATE TABLE `panier_products` (
  `panierid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `panier_products`
--

INSERT INTO `panier_products` (`panierid`, `productid`, `qty`, `date`) VALUES
(1, 2, 1, '2017-05-28 17:28:57'),
(1, 6, 1, '2017-05-28 16:01:19'),
(1, 7, 3, '2017-05-28 15:44:56'),
(1, 8, 1, '2017-05-28 15:48:37'),
(1, 11, 1, '2017-05-28 16:15:52'),
(2, 2, 10, '2017-06-03 20:07:29'),
(2, 5, 2, '2017-06-03 20:07:31'),
(2, 7, 2, '2017-06-03 20:07:33'),
(2, 8, 3, '2017-06-03 20:07:35');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `catid` int(11) NOT NULL,
  `price` float NOT NULL,
  `description` text COLLATE utf8_bin,
  `picture` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `catid`, `price`, `description`, `picture`) VALUES
(1, 'Chaîne HiFi Kenwood', 1, 500, 'Mini Chaine HiFi avec lecteur CD/DVD, bluetooth, mp3, ogg', 'kenwood.jpg'),
(2, 'PHP&MySQL Webdevelopment', 3, 49.99, 'Apprenez à créer des sites de A à Z avec grâce à PHP et MySQL !', 'php.jpg'),
(5, 'AMD Ryzen 7', 2, 350, 'Processeur AMD de dernière génération', 'ryzen7.jpg'),
(6, 'INTEL Core i7 7700k', 2, 300, 'Processeur INTEL core i7 de dernière génération', 'i7.jpg'),
(7, 'Genius Haut-Parleurs', 1, 100, 'Haut-parleurs genius pour une qualité de son incroyable !', 'geniushp.jpg'),
(8, 'Programmez en Java', 3, 39.99, 'Apprenez à programmer en Java avec ce guide complet', 'java.jpg'),
(9, 'Learn Javascript', 3, 50, 'Enrichissez vos pages avec ce language aujourd\'hui incontournable : le Javascript!\r\n', 'javascript.jpg'),
(11, 'Amplificateur Yamaha', 1, 300, 'Amplificateur Yamaha 200W', 'yamaha.jpg'),
(12, 'Design Patterns - Modèles réutilisables', 3, 59, 'Référence FR sur les design patterns', NULL),
(17, 'Phonographe Ricatech', 1, 200, 'Phonographe d\'époque Ricatech', 'phonographe.jpg'),
(18, 'TestFileExist', 1, 10, 'Test ', 'phonographe.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `products_categories`
--

CREATE TABLE `products_categories` (
  `catid` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `description` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `products_categories`
--

INSERT INTO `products_categories` (`catid`, `name`, `description`) VALUES
(1, 'hifi', 'Matériel HiFi'),
(2, 'it', 'Matériel informatique'),
(3, 'livres', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `uid` int(11) NOT NULL,
  `lastname` varchar(50) COLLATE utf8_bin NOT NULL,
  `firstname` varchar(30) COLLATE utf8_bin NOT NULL,
  `nickname` varchar(50) COLLATE utf8_bin NOT NULL,
  `address` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `postcode` int(4) DEFAULT NULL,
  `city` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `birthdate` date DEFAULT NULL,
  `blocked` tinyint(1) NOT NULL DEFAULT '0',
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `avatarurl` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `lastname`, `firstname`, `nickname`, `address`, `postcode`, `city`, `email`, `birthdate`, `blocked`, `password`, `avatarurl`, `admin`) VALUES
(5, 'admin', 'admin', 'admin', NULL, NULL, NULL, 'admin@mail.com', NULL, 0, '21232f297a57a5a743894a0e4a801fc3', NULL, 1),
(6, 'guest', 'guest', 'guest', NULL, NULL, NULL, 'guest@mail.com', NULL, 0, '1a1dc91c907325c69271ddf0c944bc72', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `usersconnections`
--

CREATE TABLE `usersconnections` (
  `uid` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `usersconnections`
--

INSERT INTO `usersconnections` (`uid`, `date`) VALUES
(5, '2017-06-11 13:21:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_messages`
--
ALTER TABLE `chat_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commentaires`
--
ALTER TABLE `commentaires`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `articleid` (`articleid`);

--
-- Indexes for table `paniers`
--
ALTER TABLE `paniers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `panier_products`
--
ALTER TABLE `panier_products`
  ADD PRIMARY KEY (`panierid`,`productid`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_categories`
--
ALTER TABLE `products_categories`
  ADD PRIMARY KEY (`catid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `usersconnections`
--
ALTER TABLE `usersconnections`
  ADD PRIMARY KEY (`uid`,`date`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `chat_messages`
--
ALTER TABLE `chat_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `commentaires`
--
ALTER TABLE `commentaires`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `paniers`
--
ALTER TABLE `paniers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `products_categories`
--
ALTER TABLE `products_categories`
  MODIFY `catid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `commentaires`
--
ALTER TABLE `commentaires`
  ADD CONSTRAINT `commentaires_ibfk_2` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`),
  ADD CONSTRAINT `commentaires_ibfk_3` FOREIGN KEY (`articleid`) REFERENCES `articles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `usersconnections`
--
ALTER TABLE `usersconnections`
  ADD CONSTRAINT `usersconnections_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
