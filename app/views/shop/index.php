<!-- Shop Index -->

<div id="wrapper">
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="sidebar-brand">Categories</li>
            <li><a href="?p=product/showcat&id=1">HiFi</a></li>
            <li><a href="?p=product/showcat&id=2">Matériel Informatique</a></li>
            <li><a href="?p=product/showcat&id=3">Livres</a></li>
        </ul>
    </div>
</div>



<div class="page-header">

    <a class="pull-right" href="?p=panier/index">
        <span class="fa fa-shopping-cart fa-2x"></span>
        <strong>Total: €<?= $total ?></strong>    
    </a>
    <?php if (!isset($_SESSION['user']->uid)): ?>
        <p class="bg-danger">Connectez-vous pour faire des achats</p>
    <?php else: ?>
        <?php if ($count): ?>
            <p>Vous avez <?= $count ?> différents produits dans votre panier</p>
        <?php else : ?>
            <p>Vous n'avez pas de produits dans votre panier</p>
        <?php endif ?>
    <?php endif; ?>
    <a href="?p=product/index"><h1>Boutique</h1></a>


</div>
<div class="container">
<!-- <img src="../assets/img/shop/1.jpeg" alt="" />-->

    <div class="row">
        <p class="lead">Catégories</p>
        <div class="list-group">
            <a href="?p=product/showCat&id=1" class="list-group-item">HiFi</a>
            <a href="?p=product/showCat&id=2" class="list-group-item">Matériel informatique</a>
            <a href="?p=product/showCat&id=3" class="list-group-item">Livres</a>
        </div>
        <?php foreach ($products as $product): ?>
            <div class="col-xs-12 col-sm-6 col-lg-4 col-md-4">
                <div class="thumbnail">
                    <img src="../assets/img/shop/<?= $product->picture; ?>" alt="">
                    <div class="caption">
                        <h4 class="pull-right">€<?= number_format($product->price, 2, ',', ' '); ?></h4>
                        <h4>
                            <a href="?p=product/show&id=<?= $product->id; ?>"><?= $product->name; ?></a>
                        </h4>
                        <p>Description : <small> <?= $product->description; ?></small></p>

                        <?php if (isset($_SESSION['user']->uid)): ?>
                            <form method="get" action="?p=panier/ajouter">
                                <!--<input type="number" min="1" name="qty" id="qte" size="2" maxlength="5" placeholder="Qté"/>-->
                                <input type="hidden" name="productid" value="<?= $product->id ?>" />
                                <input type="hidden" name="price" value="<?= $product->price ?>" />
                                <button type="submit" class="btn btn-success">Ajouter au panier</button> 
                            </form>
                        <?php endif; ?>



                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>


    <script>
        $("#menu-toggle").click(function (e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
    </script>

