<div class="page-header">
    <h2>Panier</h2>
</div>

<div class="container">
    
    <table class ="table">
        <thead>
            <th>Article</th>
            <th>Qté</th>
            <th>Prix unit.</th>
            <th>Total Article</th>
            <th>Ajouter/Supprimer</th>
        </thead>
        <?php foreach ($products as $product): ?>
        
            <tr> 
                <td><?= $product->name;?></td>
                <td><?= $product->qty;?> </td>
                <td><?=$product->price;?></td>
                <td><?=$product->total;?></td>
                <td>
                    <a href="?p=panier/delete&productid=<?= $product->id ?>"><span class="glyphicon glyphicon-remove"></span></a>
                    <a href="?p=panier/ajouter&productid=<?= $product->id ?>"><span class="glyphicon glyphicon-plus"></span></a>
                </td>    
            </tr>
                    
        <?php endforeach; ?>
            <tfoot>
               <td class="text-right" colspan="2"><strong>TOTAL €<?= $total ?></strong></td>
            </tfoot>
    </table>    
</div>




    







