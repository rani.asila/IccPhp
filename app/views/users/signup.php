<div class="container">

      <form class="form-signin" method="post">
        <h2 class="form-signin-heading">S'inscrire</h2>
        
        <label for="firstname" class="sr-only">Nom</label>
        <input type="text" name="firstname" id="firstname" maxlength="30" class="form-control" placeholder="Votre nom" required autofocus>
        
        <label for="prenom" class="sr-only">Prénom</label>
        <input type="text" name="lastname" id="lastname" maxlength="30" class="form-control" placeholder="Votre prénom" required autofocus>
        
        <label for="birthdate" class="sr-only">Date de naissance</label>
        <input type="date" name="birthdate" id="birthdate" class="form-control" placeholder="Date de naissance" required autofocus>
        
        <label for="address" class="sr-only">Adresse</label>
        <input type="text" name="address" id="address" maxlength="255" class="form-control" placeholder="Rue, numéro" required autofocus>
        
        <label for="postcode" class="sr-only">Code Postal</label>
        <input type="number" name="postcode" id="postcode" maxlength="4" class="form-control" placeholder="Code Postal" required autofocus>
        
        <label for="city" class="sr-only">Ville</label>
        <input type="text" name="city" id="city" maxlength="50" class="form-control" placeholder="Ville" required autofocus>
        
        <label for="email" class="sr-only">Email address</label>
        <input type="email" name="email" id="email" class="form-control" placeholder="Email address" required autofocus>
       
        <label for="nickname" class="sr-only">Pseudo</label>
        <input type="text" name="nickname" id="nickname" maxlength="30" class="form-control" placeholder="Pseudo" required autofocus>
        
        <label for="password" class="sr-only">Password</label>
        <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
        
        <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Envoyer</button>
      </form>

</div>