<div class="page-header">
    <h3>Mon profil</h3>
    <?php if ($_SESSION['user']->admin): ?>
        <h4>(Compte Administrateur)</h4>
    <?php endif; ?>
</div>


<div class="container">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#profil">Infos du profil</a></li>
        <li><a data-toggle="tab" href="#editProfile">Modifier mes infos de profil</a></li>
        <li><a data-toggle="tab" href="#panier">Panier</a></li>
    </ul>

    <div class="tab-content">
        <div id="profil" class="tab-pane fade in active">
            <table class="table">
                <tr>
                    <td><h4>Nom</h4></td>
                    <td><?= $_SESSION['user']->firstname; ?></td>
                </tr>
                <tr>
                    <td><h4>Prénom</h4></td>
                    <td><?= $_SESSION['user']->lastname; ?></td>
                </tr>
                <tr>
                    <td><h4>Date de naissance</h4></td>
                    <td><?= $_SESSION['user']->birthdate; ?></td>
                </tr>
                <tr>
                    <td><h4>Email</h4></td>
                    <td><?= $_SESSION['user']->email; ?></td>
                </tr>

                <tr>
                    <td><h4>Pseudo</h4></td>
                    <td><?= $_SESSION['user']->nickname; ?></td>
                </tr>
                <tr>
                    <td><h4>Adresse</h4></td>
                    <td><?= $_SESSION['user']->address . "\n" . $_SESSION['user']->postcode . " " . $_SESSION['user']->city; ?></td>
                </tr>

            </table>
        </div>

        <div id="panier" class="tab-pane fade">
            <h3>Nombre d'articles dans le panier :  <?php echo count($products); ?></h3>
            <table class ="table">
                <thead>
                <th>Article</th>
                <th>Qté</th>
                <th>Prix unit.</th>
                <th>Total Article</th>
                <th>Ajouter/Supprimer</th>
                </thead>
                <?php foreach ($products as $product): ?>

                    <tr> 
                        <td><?= $product->name; ?></td>
                        <td><?= $product->qty; ?> </td>
                        <td><?= $product->price; ?></td>
                        <td><?= $product->total; ?></td>
                        <td>
                            <a href="?p=panier/delete&productid=<?= $product->id ?>"><span class="glyphicon glyphicon-remove"></span></a>
                            <a href="?p=panier/ajouter&productid=<?= $product->id ?>"><span class="glyphicon glyphicon-plus"></span></a>
                        </td>    
                    </tr>

                <?php endforeach; ?>
                <tfoot>
                <td class="text-right" colspan="2"><strong>TOTAL €<?= $total ?></strong></td>
                </tfoot>
            </table>  
        </div> 

        <div id="editProfile" class="tab-pane fade">
            <form method="post" action="?p=user/update">
                <div class="form-group">
                    <label for="address">Adresse</label>
                    <input type="text" class="form-control" id="address" name="address" value="<?= $_SESSION['user']->address; ?>"><br>

                    <label for="postcode">Code Postal</label>
                    <input type="number"  id="lastname" name="postcode" class="form-control" value="<?= $_SESSION['user']->postcode; ?>">
                    
                    <label for="city">Ville</label>
                    <input type="text"  id="city" name="city" class="form-control" value="<?= $_SESSION['user']->city; ?>">


                    <label for="email">Email</label>
                    <input type="email"  id="email" name="email" class="form-control" value="<?= $_SESSION['user']->email; ?>">

                    <input type="hidden" id="uid" name="uid" value="<?= $_SESSION->uid; ?>" >
                    <button class="btn btn-primary pull-right" type="submit">Update</button>
                </div>
            </form>

            <br><br>
            <button data-toggle="collapse" data-target="#setPass">Modifier mon mot de passe</button>
            <br>
            <div id="setPass" class="collapse">
                <form method="post" action="?p=user/update">
                    <label for="password">Nouveau mot de passe</label>
                    <input type="password" id="password" name="password" class="form-control">
                    <input type="hidden" id="uid" name="uid" value="<?= $_SESSION['user']->uid;?>">
                    <button class="btn btn-primary pull-right" type="submit">Mettre à jour</button>

                </form>
            </div>
        </div>
    </div>
</div>












