
<div class="container">
<?php if(isset($erreur)) :?>
    <div class="alert alert-danger">
        <p>Nous avons rencontré une erreur lors de la connection, il se peut que vous soyez bloqué par l'administrateur</p>
    </div>
<?php endif; ?>
      <form class="form-signin" action="index.php?p=user/login" method="post">
        <h2 class="form-signin-heading">Login</h2>
        <label for="email" class="sr-only">Email address</label>
        <input type="email" name="email" id="email" class="form-control" placeholder="Email address" required autofocus>
        <label for="password" class="sr-only">Password</label>
        <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>
</div> <!-- /container -->


