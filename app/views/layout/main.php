<?php $app = App::getInstance(); ?>
<!DOCTYPE html>
<html>
    <head>
        <title><?= $app->title ?></title>
        <link href="../bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../bower_components/bootstrap/dist/css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/signup.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/styles.css" rel="stylesheet" type="text/css"/>
	<link href="../assets/css/sidebar.css" rel="stylesheet" type="text/css"/>
        <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/customsearch.css" rel="stylesheet" type="text/css" />
	<script src="../bower_components/jquery/dist/jquery.js" type="text/javascript"></script>
        <script src="../bower_components/bootstrap/dist/js/bootstrap.js" type="text/javascript"></script>
    </head>
    <body>
        <?php include('nav.php') ?>
        <div class="container" id="main-container">
                <?= $content ?>
        </div>
        <script type="text/javascript">
            $(function(){
               $('[data-toggle="tooltip"]').tooltip(); 
            });
        </script>
    </body>
</html>