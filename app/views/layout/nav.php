<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="?p=home/index">ICC - Projet de Développement Web 2017</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="?p=home/index">Home</a></li>
                <li><a href="?p=article/index">Blog</a></li>
                <li><a href="?p=chat/index">Chat</a></li>
                <li><a href="?p=product/index">Shop</a></li>
                <?php if (!isset($_SESSION['user']->uid)):?>
                <li><a href="?p=user/signup">S'inscrire</a></li>
                <li><a href="?p=user/login">Login</a></li>
                <?php else: ?>
                <li><a href="#" class="dropdown-toggle" data-toggle="dropdown">Hi, <?= $_SESSION['user']->nickname ?>
                        <span class="fa fa-caret-down"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="?p=user/profile&id=<?=$_SESSION['user']->uid?>">Mon profil</a></li>
                        <?php if($_SESSION['user']->admin): ?>
                        <li><a href="?p=admin/index">Administration du site</a></li>
                        <?php endif ?>
                        <li role="separator" class="divider"></li>
                        <li><a href="?p=user/logout">Logout</a></li>
                    </ul>
                </li>
                        
                        
                    
                <?php endif;?>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>