<?php use app\entities\ArticleEntity; ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3><?= $article->titre ?></h3>
    </div>
    <div class="panel-body">
        <p><?= $article->contenu ?></p>
    </div>
</div>
<h3>Commentaires</h3>
<?php
if (isset($article->commentaires)):
    foreach ($article->commentaires as $commentaire) :
        ?>
        <article>
            <h2><?= $commentaire->auteur ?></h2><small><?= $commentaire->date ?></small>
            <p><?= $commentaire->content ?></p>
        </article>
    <?php endforeach; ?>
<?php endif; ?>

<?php if (isset($_SESSION['user'])): ?>
    <form action="index.php?p=article/addComment" method="post">
        <div class="form-group">
            <label for="commentaire">Commentaire:</label>
            <textarea class="form-control" name="commentaire" rows="5" id="comment"></textarea>
        </div>
        <div class="form-group">
            <button class="btn btn-default" type="submit">Envoyer</button>
            <input type="hidden" name="articleid" value="<?= $article->id ?>">
        </div> 
    </form>

<?php endif;?>
