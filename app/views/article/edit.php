<div class="page-header"><h1>Edition Article</h1></div>
<?php var_dump($article); ?>
<div class="container">
    <form method="post" action="?p=article/update">
        <div class="form-group">
            <label for="title">Titre</label>
            <input type="text" class="form-control" id="title" name="title" value="<?= $article->titre;?>"><br>

            <label for="content">Contenu</label>
            <textarea  id="content" name="content" class="form-control" rows="15" ><?=$article->contenu;?></textarea>
            
            <input type="hidden" id="articleid" name="articleid" value="<?=$article->id;?>" >
            <button class="btn btn-primary pull-right" type="submit">Update</button>
        </div>

    </form>
</div>