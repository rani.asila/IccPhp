
<!-- Blog Index -->
<div class="container">
    <form action="?p=article/search" method="post">
        <input type="text" name="term" id="term" placeholder="Titre d'un article">
        <button class="btn btn-default" type="submit">search</button>
    </form>
    
</div>


<div class="container">
    <div class="page-header">
        <h1>Blog</h1>
    </div>
    

    <?php foreach ($articles as $article) : ?>
        <div class='col-sm-12'>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <a href="index.php?p=article/show&id=<?= $article->id ?>">
                        <?= $article->titre ?>
                    </a>

                    <small>ajouté le <?= $article->date; ?></small> 
                </div>
                <div class="panel-body">
                    <p>
                        <?= substr($article->contenu, 0, strpos($article->contenu, ' ', 300)); ?>
                    </p>
                </div>

                <?php if (isset($_SESSION['id'])) : ?>
                    <div class="panel-footer">
                        <div class="btn btn-sm btn-default">
                            Edit
                        </div>
                        <div class="btn btn-sm btn-default">
                            Delete
                        </div>

                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>







    


