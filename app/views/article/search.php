<?php var_dump($foundarticles);?>
<?php if(count($foundarticles) > 0): ?>
  
<div class="alert alert-success">
    <p><?=count($foundarticles);?> article(s) trouvé(s) pour "<?=$term;?>" : </p><br><br>
</div>
  <?php foreach ($foundarticles as $article) : ?>
        <div class='col-sm-12'>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <a href="index.php?p=article/show&id=<?= $article->id ?>">
                        <?= $article->titre ?>
                    </a>

                    <small>ajouté le <?= $article->date; ?></small> 
                </div>
                <div class="panel-body">
                    <p>
                        <?= $article->contenu ?>
                    </p>
                </div>

                <?php if (isset($_SESSION['id'])) : ?>
                    <div class="panel-footer">
                        <div class="btn btn-sm btn-default">
                            Edit
                        </div>
                        <div class="btn btn-sm btn-default">
                            Delete
                        </div>

                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php endforeach; ?>

<?php else: ?>
<div class="alert alert-warning">
    <p>Pas d'article trouvé pour "<?=$term;?>"</p>
</div>
<?php endif;?>


