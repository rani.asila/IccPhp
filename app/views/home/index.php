<div class="page-header">
    <h1>Home</h1>
</div>
<div style="background-color: white" class="container">
    <h2>Projet de Développement Web (Zaïlachi Rani, Mpindu Jean-Paul, Agadi Sam)</h2><br>
    <blockquote>
        <p><strong>/config/config.php :</strong> Paramètres de connexion à la DB<br>
        <strong>/sql/projetdevweb2017.sql :</strong> Schéma de la DB
        </p>
    </blockquote>
    <br>
        <h3>Description de l'arborescence</h3>
    <ul>
        <li>/app : Fichiers spécifiques au site(Vues, controleurs, etc.)</li>
        <li>/assets : Ressources utilisées par le site comme les fichiers css et images</li>
        <li>/bower_components : Ressources installées au moyen du gestionnaire de packages 'bower'</li>
        <li>/config : Fichier de configuration du site, se résume à un array retournant les infos de connexion à la DB</li>
        <li>/core : Partie générique du site réutilisable : Superclasses Contrôleur,  Modele dont héritent les contrôleurs et modèles spécifiques dans le dossier /app,
            ainsi qu'une classe gérant l'authentification(DbAuth.php)</li>
        <li>/public : Partie publique du site, contient index.php qui fait office de 'front controller' et de router</li>
        <li>/sql : Schéma de la DB</li>
        
    </ul>
    <h3>Comptes utilisateurs :</h3>
    <table class="table">
        <thead>
            <th>Nom</th>
            <th>Email</th>
            <th>password</th>
        </thead>
        <tr>
            <td><strong>admin</strong></td>
            <td>admin@mail.com</td>
            <td>admin</td>
        </tr>
        <tr>
            <td><strong>guest</strong></td>
            <td>guest@mail.com</td>
            <td>pass</td>
        </tr>
    </table>

</div>

      
    
