
<div id="chatbox-header">
	<h2>Chat</h2>
</div>
<div class="well" id="chatbox">
	<ul id="indexChat" class="media-list">
		<?php foreach ($messages as $message):?>
		<li
			class="media <?= $_SESSION['user']->uid == $message->uid ? 'bg-success' : '' ?>">
			<div class="media-left">
				<img src="http://placehold.it/20x20">
			</div>
			<div class="media-body">
				<h4 class="media-heading"><?= $message->nickname ?>
					<small class="pull-right"><?= $message->date ?></small>
				</h4>
				<p><?= $message->message ?></p>
			</div>
		</li>
                
		<?php endforeach;?>
        </ul>
    
	<form action="?p=chat/add" id="chatform" method="post">
		<div class="form-group" id="msg-group">
			<label class="control-label" for="message">Message</label>
			<textarea class="form-control" name="message" id="message"></textarea>
			<button class="btn btn-primary" id="btn-form" type="submit">Envoyer</button>
		</div>
	</form>
</div>



<script type="text/javascript">
    $(function(){
        console.log("test");
        $('#chatform').on('submit', function(event){
            console.log($('#message').val());
            $.ajax({
                url: "?p=chat/add",
                method: "post",
                data: "message="+$('#message').val(),
                //dataType: 'json',
                success: function(response){
                    console.log('Réponse AJAX ' + response);
                    $('#indexChat').append(response);
                    
                    
                },
                error: function(error){
                    console.log(error);
                }
            });
            return false;
        });
        setInterval(function(){
            $.get('?p=chat/getMessages', function(response){
                $('#indexChat').html(response);
            });
    
        }, 5000);
       
    });
                
                

</script>