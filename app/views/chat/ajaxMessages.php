	
<?php foreach ($messages as $message): ?>
    <li
        class="media <?= $_SESSION['user']->uid == $message->uid ? 'bg-success' : '' ?>">
        <div class="media-left">
            <img src="http://placehold.it/20x20">
        </div>
        <div class="media-body">
            <h4 class="media-heading"><?= $message->nickname ?>
                <small class="pull-right"><?= $message->date ?></small>
            </h4>
            <p><?= $message->message ?></p>
        </div>
    </li>
<?php endforeach; ?>
