<!-- Admin Index -->

<div class="container">
    <div class="page-header">
        <span class="glyphicon glyhicon-dashboard"></span><h1>Dashboard</h1>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-lg-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>Blog</h3>
                </div>
                <div class="panel-body">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="#" data-toggle="modal" data-target="#listArticles"> 
                                #articles
                            </a>
                            <span class="badge"><?= count($articles) ?></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <span data-toggle="modal" data-target="#addArticle">
                                    <span class="glyphicon glyphicon-plus-sign"></span>
                                    Ajouter un article
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-lg-4">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>Membres</h3>
                </div>
                <div class="panel-body">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="#" data-toggle="modal" data-target="#listUsers">
                                #membres
                            </a>
                            <span class="badge"><?= count($users) ?></span>
                        </li>
                    </ul>    

                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-lg-4">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>Shop</h3>
                </div>
                <div class="panel-body">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="#" data-toggle="modal" data-target="#listProducts">
                                #produits en magasin
                            </a>
                            <span class="badge"><?= count($products) ?></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <span data-toggle="modal" data-target="#addProduct">
                                    <span class="glyphicon glyphicon-plus-sign"></span>
                                    Ajouter un produit
                                </span>
                            </a>   
                        </li>
                    </ul>    

                </div>
            </div>
        </div>

    </div>
</div>


<div class="modal fade" id="listArticles">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><h3>Liste des articles</h3></div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                    <th>Titre</th>
                    <th>Date</th>
                    <th>Opérations</th>
                    </thead>
                    <?php foreach ($articles as $article): ?>
                        <?php
                        $tmpcontent = $article->contenu;
                        $tmptitle = $article->titre;
                        ?>
                        <tr>
                            <td><?= $article->titre; ?></td>
                            <td><?= $article->date; ?></td>
                            <td>
                                <a href="?p=article/delete&id=<?= $article->id; ?>"><span class="glyphicon glyphicon-remove"></span></a>
                                <a href="?p=article/edit&id=<?= $article->id; ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                            </td>
                        </tr>



                    <?php endforeach; ?>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Fermer
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addArticle">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><h3>Ajouter un article</h3></div>
            <form method="post" action="?p=article/add">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="title">Titre</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="Titre de l'article"><br>
                    </div>
                    <div class="form-group">
                        <label for="content">Contenu</label>
                        <textarea  id="content" name="content" class="form-control" rows="15" placeholder="Votre article"></textarea>
                    </div> 


                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary pull-right" type="submit">Enregistrer</button>
            </form>
        </div>
    </div>
</div>
</div>


<div class="modal fade" id="listUsers">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><h3>Liste des membres</h3></div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Pseudo</th>
                    <th>Dernière connexion</th>
                    <th>Opérations</th>
                    </thead>
                    <?php foreach ($users as $user): ?>

                        <tr>
                            <td><?= $user->firstname; ?></td>
                            <td><?= $user->lastname; ?></td>
                            <td><?= $user->nickname; ?></td>
                            <th><?= $user->uid; ?></th>
                            <td>
                                <a href="?p=user/show&id=<?= $user->uid; ?>" data-toggle="tooltip" title="Gestion du profil">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                                <?php if ($user->blocked): ?>
                                    <a href="?p=user/switchlock&id=<?= $user->uid; ?>">
                                        <span class="glyphicon glyphicon-lock"></span>
                                    </a>
                                <?php else: ?>
                                    <a href="?p=user/block&id=<?= $user->uid; ?>">
                                        <span class="glyphicon glyphicon-ban-circle"></span>
                                    </a>
                                <?php endif ?>
                                <a href="?p=user/delete&id=<?= $user->uid; ?>">
                                    <button class="btn btn-danger">Supprimmer</button>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>



                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Fermer
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="listProducts">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><h3>Liste des produits</h3></div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                    <th>Nom</th>
                    <th>Description</th>
                    <th>Prix</th>
                    </thead>
                    <?php foreach ($products as $product): ?>
                        <tr>
                            <td>
                                <a href="?p=product/show&id=<?= $product->id; ?>" target="_blank">
                                    <?= $product->name; ?>

                                </a>                            </td>
                            <td><?= $product->description; ?></td>
                            <td><?= $product->price; ?></td>
                            <td>
                                <a href="?p=product/delete&id=<?= $product->id; ?>">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Fermer
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addProduct">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><h3>Ajouter un produit</h3></div>
            <form method="post" action="?p=product/add" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Nom</label>
                        <input type="text" class="form-control" id="title" name="name" placeholder="Denomination du produit"><br>
                    </div>
                    <div class="form-group">
                        <label for="price">Prix</label>
                        <input type="number" class="form-control" id="price" name="price" placeholder="Prix"><br>
                    </div>
                    <div class="form-group">
                        <label for="categorie">Categorie</label>
                        <select name="categorie">
                            <option value="1">HiFi</option>
                            <option value="2">Informatique</option>
                            <option value="3">Livres</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="image">Choisissez une image pour le produit (.jpg, .jpeg, .png ou .gif < 1mB): </label>
                        <input type="hidden" name="MAX_FILE_SIZE" value="1000000">
                        <input type="file" name="image" id="image">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea id="description" name="description" class="form-control" rows="5" placeholder="Description"></textarea>
                    </div>
                </div>
                <div class="panel panel-footer">
                    <div class="form-group">
                        <button class="btn btn-primary pull-right" type="submit">Ajouter</button>
                    </div>
                </div>
            </form>


        </div>
    </div>
</div>









