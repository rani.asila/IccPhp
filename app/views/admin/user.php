<?php var_dump($lastconnections); ?>

<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#profil">Infos du profil</a></li>
    <li><a data-toggle="tab" href="#blog">Blog</a></li>
    <li><a data-toggle="tab" href="#paniers">Paniers</a></li>
    <li><a data-toggle="tab" href="#connections">Connections</a></li>
</ul>

<div class="tab-content">
    <div id="profil" class="tab-pane fade in active">
        <table class="table">
            <tr>
                <td><h4>Nom</h4></td>
                <td><?= $user->lastname; ?></td>
            </tr>
            <tr>
                <td><h4>Prénom</h4></td>
                <td><?= $user->lastname; ?></td>
            </tr>
            <tr>
                <td><h4>Date de naissance</h4></td>
                <td><?= $user->birthdate; ?></td>
            </tr>
            <tr>
                <td><h4>Email</h4></td>
                <td><?= $user->email; ?></td>
            </tr>

            <tr>
                <td><h4>Pseudo</h4></td>
                <td><?= $user->lastname; ?></td>
            </tr>
            <tr>
                <td><h4>Adresse</h4></td>
                <td><?= $user->address . "\n" . $user->postcode . " " . $user->city; ?></td>
            </tr>



        </table>
    </div>

    <div id="blog" class="tab-pane fade">
        <h3>5 Derniers commentaires postés</h3>
        <?php if (count($comments) > 0): ?>
            <?php foreach ($comments as $comment): ?>
                <div class="panel panel-default">
                    <div class="panel panel-heading">
                        <h4>Posté le <?= $comment->date; ?></h4>
                    </div>
                    <div class="panel panel-body">
                        <p><?= $comment->content; ?></p>
                    </div>

                </div>
            <?php endforeach; ?>
        <?php endif; ?>
        <div id="menu3" class="tab-pane fade">
            <h3>Connections</h3>
            <p>Some content in menu 2.</p>
        </div>
    </div>

    <div id="paniers" class="tab-pane fade">
        <div class="container">
            <h3>Actuellement dans le panier</h3>
            <table class="table">
                <?php foreach ($products as $product): ?>
                    <tr>
                        <td><?= $product->name; ?></td>
                        <td><?= $product->qty; ?> </td>
                        <td><?= $product->price; ?></td>
                        <td><?= $product->total; ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
    
     <div id="connections" class="tab-pane fade">
        <div class="container">
            <h3>Connexions les 7 derniers jours</h3>
            <p><strong><?= $user->firstname . " " . $user->lastname;?></strong> s'est connecté aux dates suivantes :</p>
            <?php foreach ($lastconnections as $connection):?>
            <h4><?=$connection->date;?></h4>
            <?php endforeach;?>
                
        </div>
    </div>
