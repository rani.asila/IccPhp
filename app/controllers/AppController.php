<?php

namespace app\controllers;

use App;
use core\controllers\Controller;
use const ROOT;

/**
 * Contrôleur principal de l'application.
 * Ici est déterminé le template utilisé pour le site, ainsi que le répertoire
 * par défaut contenant les vues.
 *
 */
class AppController extends Controller {

    /**
     * Constructeur : détermination du répertoire par défaut des vues et du template par défaut
     */
    public function __construct() {
        parent::__construct();
        $this->viewPath = ROOT . '/app/views/';
        $this->template = 'main';
    }

    /**
     * Charger un modèle (i.e. obtenir un accès à une table)
     * @param string $modele Le nom du modèle à charger (par exemple: "Vehicules" pour le modèle 'VehiculesModel'
     */
    protected function loadModel($modele) {
        $this->$modele = App::getInstance()->getModel($modele);
        //Ou $this->modele = $app->getModel($modele)
    }
    
    /**
     * v1 : 
     * Charge un fichier d'un formulaire et vérifie si le fichier est une image de moins d'1mB
     * Le champs 'name' du input doit avoir pour valeur 'image'
     * @todo Personnaliser les messages d'erreur en fct du type d'erreur
     * @param type $dir
     * @return boolean
     */
    public function imgUpload($dir){
        $uploadDir = ROOT .'/assets/img/'. $dir;
        $uploadFile = $uploadDir . "/" . basename($_FILES['image']['name']);
        $imgTypes = ['image/png', 'image/jpg', 'image/jpeg', 'image/gif'];
        
       
        if(is_uploaded_file($_FILES['image']['tmp_name'])){
          if(move_uploaded_file($_FILES['image']['tmp_name'], $uploadFile)
             && in_array($_FILES['image']['typpe'], $imgTypes)
             && !file_exists($uploadFile)){
                return "File successfully uploaded";
           }else{
                return $_FILES['image']['error'];
           }
        }else{
            return $_FILES['image']['error'];
        }
    }
                
            
         
         
        
        
        
    
    /*
     * v2:
     * 
     * public function imgUpload($dir, $filename) {
        $target_dir = "/assets/img/" . $dir;
        $target_file = $target_dir . basename($_FILES[$filename]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

        if (isset($_POST["submit"])) {
            $check = getimagesize($_FILES[$filename]["tmp_name"]);
            if ($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
        // Check file size
        if ($_FILES[$filename]["size"] > 1000000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES[$filename]["tmp_name"], $target_file)) {
                echo "The file " . basename($_FILES[$filename]["name"]) . " has been uploaded.";
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
    }     
    */

}
