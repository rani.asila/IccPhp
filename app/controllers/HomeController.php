<?php

namespace app\controllers;

use const ROOT;

class HomeController extends AppController {
    
    
    public function index() {
     
        $this->render('home/index'); 
    }
}
