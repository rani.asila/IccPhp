<?php

namespace app\controllers;

class ProductController extends AppController {
	
	public function __construct(){
		parent::__construct();
		$this->loadModel('Product');
                $this->loadModel('PanierProducts');
	}
	
	public function index(){
            $panierproducts = $this->PanierProducts->getProducts($_SESSION['panier']->id);
            $total = 0;
            foreach ($panierproducts as $product) {
                $total += $product->total;
            }
            $count = count($panierproducts);
            $products = $this->Product->all();
            $this->render('shop/index', ['products' => $products, 
                                         'count' => $count, 
                                         'total' => $total]);
	}
        
        public function add(){
            $uploadOK = $this->imgUpload("shop");
            $product = ['name'          => htmlspecialchars($_POST['name']),
                        'price'         => htmlspecialchars($_POST['price']),
                        'catid'         => htmlspecialchars($_POST['categorie']),
                        'description'   => htmlspecialchars($_POST['description']),
                        'picture'       => $_FILES['image']['name']];
                    
            $this->Product->create($product);
            header('Location: ' . $_SERVER['HTTP_REFERER']);

        }
        
        public function delete(){
            $this->Product->delete($_GET['id']);
            header('Location: ' . $_SERVER['HTTP_REFERER']);

        }
	
	public function show(){
		$product = $this->Product->find($_GET['id']);
		$this->render('shop/show', ['product' => $product]);
	}
	
	public function showCat(){
		$id = $_GET['id'];
                $products = $this->Product->db->prepare("SELECT * FROM products WHERE `catid` = ? ", [$id], get_called_class(), false);
		$this->render('shop/index', ['products' => $products]);
		
	}
        
       
}