<?php

namespace app\controllers;

use App;
use core\auth\DbAuth;

class UserController extends AppController {

    public function __construct() {
        parent::__construct();

        $this->loadModel('User');
        $this->loadModel('Commentaire');
        $this->loadModel('Product');
        $this->loadModel('Panier');
        $this->loadModel('PanierProducts');
    }

    public function home() {
        $users = $this->User->all();
    }

    public function login() {
        if (!empty($_POST)) {
            $auth = new DbAuth(App::getInstance()->getDb());
            if ($auth->login(htmlspecialchars($_POST['email']), 
                             htmlspecialchars($_POST['password']))
               ){
                header('Location:index.php?p=article/index');
            } else {
                $erreur = true;
                $this->render('users/login', ['erreur' => $erreur]);
            }
        } else {
            $this->render('users/login');
        }
    }

    public function logout() {
        session_destroy();
        unset($_SESSION);
        session_start();
        $this->render('users/login');
    }

    public function signup() {
        if (!empty($_POST)) {

            $this->User->create([
                'firstname' => htmlspecialchars($_POST['firstname']),
                'lastname'  => htmlspecialchars($_POST['lastname']),
                'birthdate' => htmlspecialchars($_POST['birthdate']),
                'nickname'  => htmlspecialchars($_POST['nickname']),
                'address'   => htmlspecialchars($_POST['address']),
                'postcode'  => htmlspecialchars($_POST['postcode']),
                'city'      => htmlspecialchars($_POST['city']),
                'email'     => htmlspecialchars($_POST['email']),
                'password'  => md5(htmlspecialchars($_POST['password']))
                ]);
            $this->login();
        }
        $this->render('users/signup');
    }

    public function show() {
        $user = $this->User->find($_GET['id']);
        $panier = $this->Panier->getByUserId($_GET['id']);
        $products = $this->PanierProducts->getProducts($panier->id);
        $comments = $this->Commentaire->getLastComments($_GET['id']);
        
        $nbdays = 7;
        $lastconnections = $this->User->getLastConnections($_GET['id'], $nbdays);
        $this->render('admin/user', ['lastconnections' => $lastconnections, 
                                     'user' => $user,
                                     'comments' => $comments,
                                     'products' => $products]);
    }
    
    public function update(){
        if(isset($_POST['password'])){
            $this->User->update($_SESSION['user']->uid,
                               ['password' => md5($_POST['password'])]);
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }else{
            $this->User->update($_SESSION['user']->uid,
                               ['address'  => htmlspecialchars($_POST['address']),
                                'postcode' => htmlspecialchars($_POST['postcode']),
                                'city'     => htmlspecialchars($_POST['city'])
                               ]);
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
    }
            

    
    
    public function profile(){
        $products = $this->PanierProducts->getProducts($_SESSION['panier'] -> id);
        $this->render('users/profile', ['products' => $products]);
    }
        

}
