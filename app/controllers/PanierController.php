<?php

namespace app\controllers;

use app\models\Panier;

class PanierController extends AppController {

    public function __construct() {
        parent::__construct();
        $this->loadModel('Product');
        $this->loadModel('Panier');
        $this->loadModel('PanierProducts');
    }

    public function index() {
        $products = $this->PanierProducts->getProducts($_SESSION['panier']->id);
        $total = 0;
        foreach ($products as $product) {
            $total += $product->total;
        }
        $this->render('shop/panier', ['products' => $products, 'total' => $total]);
    }

    public function ajouter() {
        
        if (isset($_POST['productid'])) {
            $productid = $_POST['productid'];
        } else if (isset($_GET['productid'])) {
            $productid = htmlspecialchars($_GET['productid']);
        } else {
            $this->notFound();
        }
        
        /*
         * On vérifie si le produit est déjà présent dans le panier
         * Si oui, on incrémente la qté, sinon on introduit le produit dans le panier
        */
        
        $product = $this->PanierProducts->getProductByPanierId([$_SESSION['panier']->id, $productid]);
        if ($product) {
            if($product->qty < 10){
                $this->PanierProducts->addProductQty([$_SESSION['panier']->id, $productid]);
            }else{
                echo "Vous avez atteint la quantité maximum(10) pour ce produit";
            }
        } else {
            $this->PanierProducts->create([
                'panierid' => $_SESSION['panier']->id,
                'qty' => '1',
                'productid' => $productid]);
        }
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }

    public function delete() {
        $this->PanierProducts->remove($_SESSION['panier']->id, htmlspecialchars($_GET['productid']));
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }

}
