<?php

namespace app\controllers;

class CommentaireController extends AppController{
	
	public function __construct(){
		parent::__construct();
	}
	
	
	
	public function add($commentaire){
		$commentaire = ['idBillet' => $_POST['idBillet'],
                                'auteur' => $_SESSION['name'],
				'date' => date(now),
			        'contenu' => htmlspecialchars($_POST['content'])];
		$this->commentaire->create($commentaire);
	}
}