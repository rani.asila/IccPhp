<?php

//test remote

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers;

/**
 * Description of AdminController
 *
 * @author rani
 */
class AdminController extends AppController {
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->loadModel('Article');
        $this->loadModel('User');
        $this->loadModel('Product');
    }
    
    public function index(){
        $articles = $this->Article->all();
        $products = $this->Product->all();
        $users = $this->User->all();
        $this->render('admin/index', ['articles' => $articles, 
                                      'users' => $users,
                                      'products' => $products ]);
    }
    
    
    
}
