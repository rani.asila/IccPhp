<?php

/*
 * Controleur principal de la partie Blog 
 */

namespace app\controllers;

use app\models\Article;

class ArticleController extends AppController {

    public function __construct() {
        parent::__construct();

        $this->loadModel('Article');
        $this->loadModel('Commentaire');
    }

    public function index() {
        $articles = $this->Article->all();
        $this->render('article/index', ['articles' => $articles]);
    }


    public function edit() {
        $article = $this->Article->find(htmlspecialchars($_GET['id']));
        $this->render('article/edit', ['article' => $article]);
    }

    public function update() {
        $this->Article->update(htmlspecialchars($_POST['articleid']), 
                             ['titre'   => htmlspecialchars($_POST['title']),
                              'contenu' => htmlspecialchars($_POST['content'])]);
        header('Location: ?p=admin/index');
    }

    public function search() {
        $articles = $this->Article->all();
        $term = htmlspecialchars(strtolower(trim($_POST['term'])));
        $foundarticles = $this->Article->search($term);

        $this->render('article/search', ['foundarticles' => $foundarticles,
                                         'articles'     => $articles,
                                         'term' => $term]);

    }

    public function show() {
        $article = $this->Article->find(htmlspecialchars($_GET['id']));
        $article->commentaires = $this->Commentaire->getByArticleId($article->id);
        $this->render('article/show', ['article' => $article]);
    }

    public function add() {
        $this->Article->create([
                'titre'   => htmlspecialchars($_POST['title']), 
                'contenu' => htmlspecialchars($_POST['content'])
                ]);
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }

    public function delete() {
        $articleid = htmlspecialchars($_GET['id']);
        $this->Article->delete($articleid);
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }

    public function addComment() {
        $this->Commentaire->create([
            'content' => htmlspecialchars($_POST['commentaire']),
            'uid' => $_SESSION['user']->uid,
            'articleid' => htmlspecialchars($_POST['articleid'])
            ]);
        $this->index();
    }

}
