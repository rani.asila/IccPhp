<?php

namespace app\controllers;


class ChatController extends AppController{
	
	public function __construct(){
		parent::__construct();
		$this->loadModel('Chat');
	}
	
	public function index(){
		$messages = $this->Chat->all();
		$this->render('chat/index', ['messages' => $messages]);
	}
	
	public function add(){
		$message = ['message' => htmlspecialchars($_POST['message']),
                            'uid'     => $_SESSION['user']->uid];
		$this->Chat->create($message);
                
	
                $chatMessage = '<li class="media bg-success">'
                                    .'<div class="media-left">'
                                        .'<img src="http://placehold.it/20x20">'
                                    .'</div>'
                                    .'<div class="media-body">'
                                        .'<h4 class="media-heading">'.$_SESSION['user']->nickname
                                        .'<small class="pull-right">'.date('d/m/Y H:i:s'). '</small>'
                                    .'</h4>'
                                        .'<p>' . $message['message'] . '</p>'
                                    .'</div>'
                                .'</li>';
                echo $chatMessage;
                
                /*$tab = ['message' => 'salut', 'statut' => 'ok'];
                echo json_encode($tab);*/
        }
        
        public function getMessages(){
            $messages = $this->Chat->all();
            echo $this->renderPartial('chat/ajaxMessages', ['messages' => $messages]);
        }
	
	/*
	 * public function testAjax() {
		$this->create(['message' => $_POST['message'], 'uid' => $_SESSION['uid']]);
		$response = ['uid' => $_SESSION['uid'], 'message' => $_POST['message']];
		echo json_encode($response);
	}
	**/
}
		
		