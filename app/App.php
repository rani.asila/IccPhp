<?php

use core\Config;
use core\db\Db;

/**
 * Description of App
 */
class App {
    /* v1 (sans classe Config)
      const DB_HOST = 'mysql:host=localhost';
      const DB_NAME = 'blog';
      const DB_USER = 'root';
      const DB_PASS = 'root';
     */

    private $db_instance;
    private static $_instance;
    
    // Titre de la page, dynamique
    public $title = 'ProjetDevWeb';

    public static function load() {
        require ROOT . '/app/Autoloader.php';
        app\Autoloader::register();
        require ROOT . '/core/Autoloader.php';
        core\Autoloader::register();
        session_start();
    }

    /**
     * Singleton pattern
     * Renvoie une instance de la classe 'App', le singleton pattern prévient 
     * l'instanciation de plusieurs objets
     * @return app\App instance
     */
    public static function getInstance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new App();
        }
        return self::$_instance;
    }

    /**
     * Get access to a model
     * @param type $name
     * @return \className
     */
    public function getModel($name) {
        $className = '\\app\\models\\' . ucfirst($name);
        return new $className($this->getDb());
    }

    /* Version 2 */

    public function getDb() {
        if (is_null($this->db_instance)) {
            $config = Config::getInstance(ROOT . '/config/config.php');
            $this->db_instance = new Db($config->get('dbHost'), $config->get('dbName'), $config->get('dbUser'), $config->get('dbPass'));
         // ou $this->db_instance = new Db(self::DBHOST, self::DBNAME,...
        }
        return $this->db_instance;
    }

    /* Version 1
      public static function getDb() {
      if (self::$database === null) {
      self::$database = new Database(self::DB_HOST, self::DB_NAME, self::DB_USER, self::DB_PASS);
      }
      return self::$database;
      } */

    public function getTitle() {
        return $this->title;
    }

    public function setTitre($titre) {
        $this->titre .= ' | ' . $titre;
    }

}
