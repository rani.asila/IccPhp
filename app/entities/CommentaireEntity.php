<?php

namespace app\entities;
use core\entities\Entity;
use app\models\User;
use App;

class CommentaireEntity extends Entity{
	
	public function getAuteur(){
		$user = new User(App::getInstance()->getDb());
		return $user->find($this->uid)->nickname;
	}
}