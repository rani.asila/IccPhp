<?php


namespace app\entities;

use core\entities\Entity;

class PanierProductsEntity extends Entity{
    
    public function getTotal(){
        return $this->price * $this->qty;
    }
}
