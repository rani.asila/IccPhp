<?php



namespace app\entities;

use core\entities\Entity;
use app\models\PanierProducts;

class PanierEntity extends Entity{
    
    
    public function getProducts($productid) {
        
        $panierproduct = new PanierProducts(\App::getInstance()->getDb());
        return $panierproduct->getProductByPanierId();
        
    }
}
