<?php

namespace app\models;

use core\db\Db;
use core\models\Model;

class Article extends Model{
    
    
    public function search($term){
        $param = "%{$term}%";
        
        return $this->requete("SELECT * from {$this->table} WHERE LOWER(titre) LIKE ? ORDER BY id DESC", [$param]);
    }
}
