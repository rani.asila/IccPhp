<?php

namespace app\models;

use core\models\Model;

class UsersConnections extends Model {
    
	
	public function find($id) {
		return $this->requete("SELECT * FROM {$this->table} WHERE uid = ?"
		, [$id], get_called_class(), true);
	}
        
        public function getLastConnections($userid, $nbdays){
            return $this->requete("SELECT * FROM {$this->table}  WHERE uid = ? "
                                . "AND TIMESTAMPDIFF(DAY, now(), date) <= ?", [$userid, $nbdays]);
        }
    
}