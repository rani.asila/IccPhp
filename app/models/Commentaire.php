<?php

namespace app\models;
use core\models\Model;

class Commentaire extends Model{
	
	public function getByArticleId($id){
		
		return $this->requete("SELECT * FROM {$this->table} WHERE articleid = ?", [$id], get_called_class());
	}
	
        
        public function getLastComments($id){
                return $this->requete("SELECT * FROM {$this->table} WHERE uid = ? ORDER BY id desc LIMIT 5", [$id], get_called_class());
        }
	
}