<?php

namespace app\models;

use core\models\Model;

class Panier extends Model {

    public function getByUserId($id) {
        return $this->requete("SELECT * FROM {$this->table} WHERE uid = ?", [$id], get_called_class(), true);
    }

}
