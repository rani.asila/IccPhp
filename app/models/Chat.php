<?php

namespace app\models;
use core\models\Model;
use core\db\Db;
use App;

class Chat extends Model{
	
	public function __construct(Db $db){
		parent::__construct(App::getInstance()->getDb());
		$this->table = 'chat_messages';
	}
        
        public function all(){
            return $this->requete("(SELECT m.message, m.uid, m.date, u.nickname FROM chat_messages m INNER JOIN users u "
                                . "ON m.uid = u.uid ORDER BY m.date desc LIMIT 10) order by date asc", null, get_called_class());
        }
	
}
	
	
	
	