<?php

namespace app\models;

use core\models\Model;

class User extends Model {
    
	
	public function find($id) {
		return $this->requete("SELECT * FROM {$this->table} WHERE uid = ?"
		, [$id], get_called_class(), true);
	}
        
        public function getLastConnections($userid, $nbdays){
            return $this->requete("SELECT * FROM usersconnections WHERE uid = ? "
                                . "AND TIMESTAMPDIFF(DAY, now(), date) <= ?", [$userid, $nbdays]);
        }
        
        public function update($id, $champs) {
        // Tableau des champs
        $sql_parts = [];
        // Tableau des valeurs
        $attributs = [];

        foreach ($champs as $cle => $val) {
            $sql_parts[] = "$cle = ?";
            $attributs[] = $val;
        }
        $attributs[] = $id; //L'id est ajouté à la fin du tableau existant

        // Concaténation des bouts de requete avec une virgule entre chaque champ
        $sql_part = implode(',', $sql_parts);
        
        return $this->requete("UPDATE {$this->table} SET $sql_part"
        					  ."WHERE uid = ?"
        					  , $attributs, true);
    }
    
}
