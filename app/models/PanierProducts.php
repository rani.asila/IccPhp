<?php

namespace app\models;

use core\models\Model;
use App;

class PanierProducts extends Model {

    public function __construct(\core\db\Db $db) {
        parent::__construct(App::getInstance()->getDb());
        $this->table = "panier_products";
    }

    
    
    public function getProductByPanierId($attributs) {
        return $this->requete("SELECT * FROM {$this->table} WHERE panierid = ? AND productid = ?", $attributs, get_called_class(), true);
    }

    /**
     * @TODO Placer getProducts() dans modele Panier
     */
    public function getProducts($panierid) {
        return $this->requete("SELECT p.name, p.id, p.price, pp.qty FROM {$this->table} as pp INNER JOIN products as p "
                        . "ON pp.productid = p.id WHERE pp.panierid = ?", [$panierid]);
    }
    
   
    public function getProductsCount($panierid){
        return $this->requete("SELECT COUNT(*) as cnt FROM {$this->table} "
                            . "WHERE panierid = ?", [$panierid], get_called_class(), true);
    }

    /**
     * 
     * @param type $attributs tableau contenant (dans l'ordre):
     *  - la qte à ajouter
     *  - l'id du panier
     *  - l'id du produit
     * @return boolean
     */
    public function addProductQty($attributs) {
        return $this->requete("UPDATE {$this->table} SET qty = qty + 1 WHERE panierid = ? AND productid = ?", $attributs, true);
    }

    public function remove($panierid, $productid) {
        $product = $this->getProductByPanierId([$panierid, $productid]);
        if ($product->qty == 0) {
            
            return $this->requete("DELETE FROM {$this->table} WHERE panierid = ? AND productid = ?", [$panierid, $productid]);
        } else {
            return $this->requete("UPDATE {$this->table} SET qty = qty - 1 WHERE panierid = ? AND productid = ?", [$panierid, $productid]);
        }
        
    }


    /**
     * 
     * @param type $productid
     * @return type int 
     * Renvoie le prix total par produit
     */
    public function getTotalByProductId($productid) {
        $price = $this->requete("SELECT price FROM products WHERE productid = ?", [$productid], get_called_class(), true);
        return $price * $this->requete("SELECT qty from {$this->table} "
                        . "WHERE panierid = ? AND productid = ?", [$_SESSION['panier']->id, $productid], get_called_class(), true);
    }

}
