<?php

namespace core\entities;

class Entity {

    /**
     * Méthode "magique" qui permet d'appeler la méthode get$key() lorsqu'on appelle $this->$key
     * @param string $key
     * @return method
     */
    
    public function __get($key) {
        $method = 'get' . ucfirst($key);
        $this->$key = $this->$method();
        return $this->$key;
    }

}
