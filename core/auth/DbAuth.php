<?php

namespace core\auth;

use core\db\Db;

/**
 * Classe représentant une connexion par db
 */
class DbAuth {

    private $db;

// Injection de dépendance
    public function __construct(Db $db) {
        $this->db = $db;
    }

    /**
     * 
     * @param string $login
     * @param string $pass
     * @return bool
     */
    public function login($email, $pass) {
        $user = $this->db->prepare(""
                . "SELECT * "
                . "FROM users "
                . "WHERE email = ? AND password = ?", [$email, md5($pass)], null, true);
        $paniermodel = new \app\models\Panier($this->db);
        if ($user && !$user->blocked) {
            $_SESSION['user'] = $user;
            $panier = $paniermodel->getByUserId($user->uid);
            $this->db->prepare("INSERT INTO usersconnections (uid) values (?)", [$_SESSION['user']->uid]);
            if($panier){
                $_SESSION['panier'] = $panier;
            } else {
                $id = $paniermodel->create(['uid' => $user->uid]);
                $panier = $paniermodel->getByUserId($id);
                $_SESSION['panier'] = $panier;
            }
            return true;
        }
        return false;
    }
            
                


    public function signup($email, $password) {
        return $this->db->prepare("INSERT INTO users (email,password) VALUES(?,?)", [$email, md5($password)], null, true);
    }

}
