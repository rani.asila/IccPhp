<?php

namespace core\models;

use core\db\Db;

/**
 * Modèle 'parent' de l'application. 
 * Gère les opérations avec la base de données : Ajout, Recherche, Mise à jour,
 * Suppression (CRUD en anglais).
 *
 */
class Model {

    /**
     * La table sur laquelle exécuter la requête
     * @var string
     */
    protected $table;

    /**
     * La base de données
     * @var Db
     */
    public $db;

    /**
     * Extraction du nom de la table à partir du nom de la classe.
     * Si la table n'est pas encore définie, on récupère la dernière partie
     * du nom de la classe, namespace compris 
     * @param Db $db
     */
    public function __construct(Db $db) {

        $this->db = $db;

        if (is_null($this->table)) {

            $parts = explode('\\', get_class($this));
            $className = end($parts);
            $this->table = strtolower($className).'s';
        }
    }

    /**
     * Requête vers la db.
     * S'il y a des paramètres, on effectue une requête préparée, sinon une simple.
     * On retourne un objet de la classe ...Entite correspondant à la classe ...Model 
     * ayant effectué la requête. (ex : VehiculesModel --> VehiculesEntite);
     * Ainsi, on allège la réponse -> Une entité ne contient pas les données de connexion à
     * la Db
     * 
     * @param type $sql La requête à effectuer
     * @param type $params Les éventuels paramètres
     * @param type $one Résultat unique ?
     * @return Entite
     */
    public function requete($sql, $params = null, $className = null, $one = false) {
        $className = str_replace("models", "entities", get_class($this) . 'Entity');
        if ($params){
            return $this->db->prepare($sql, $params, $className , $one);
        }else{
            return $this->db->requete($sql, $className, $one);
        }
    }

    /**
     * Mise à jour d'un enregistrement
     * @param int $id       Id de l'enregistrement
     * @param array $champs Champs à modifier
     */
    public function update($id, $champs) {
        // Tableau des champs
        $sql_parts = [];
        // Tableau des valeurs
        $attributs = [];

        foreach ($champs as $cle => $val) {
            $sql_parts[] = "$cle = ?";
            $attributs[] = $val;
        }
        $attributs[] = $id; //L'id est ajouté à la fin du tableau existant

        // Concaténation des bouts de requete avec une virgule entre chaque champ
        $sql_part = implode(',', $sql_parts);
        
        return $this->requete("UPDATE {$this->table} SET $sql_part"
        					  ."WHERE id = ?"
        					  , $attributs, true);
    }

    /**
     * Création d'un enregistrement
     * 
     * @param type $champs Les champs à remplir
     * @return bool
     */
    public function create($champs) {
        // Tableau des champs
        $fields = [];
        // Tableau des valeurs
        $values = [];
        foreach ($champs as $cle => $val) {
            $fields[] = "$cle = ?";
            $values[] = $val;
        }
        // Concaténation des bouts de requete avec une virgule entre chaque champ
        $field = implode(',', $fields);
        if($this->requete("INSERT INTO {$this->table} SET $field", $values, true)){
            return $this->lastId();
        }else{
            return false;
        }
    }

    /**
     * Suppression d'un enregistrement
     * @param int $id
     * @return bool
     */
    public function delete($id) {
        return $this->requete("DELETE FROM {$this->table} WHERE id = ?", [$id]);
    }

    /**
     * Retourne un élément dans la table correspondant à la classe appelée
     * 
     * @param int $id L'id de l'enregistrement à récupérer
     * @return Entite
     */
    public function find($id) {
        return $this->requete("SELECT * FROM {$this->table} WHERE id = ?"
		                      , [$id], get_called_class(), true); 
    }
		                     
    /**
     * Récupérer tous les enregistrements d'une table.
     * @return recordset
     */
    public function all() {
        return $this->requete("SELECT * FROM " . $this->table);
    }

    /**
     * Récupérer l'id du dernier enregistrement sauvegardé dans la db
     * @return int
     */
    public function lastId() {
        return $this->db->lastInsertId();
    }
}
		                       


